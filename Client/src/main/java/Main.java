import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import proto.PersonOuterClass;
import proto.PersonServiceGrpc;

import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();

        PersonServiceGrpc.PersonServiceStub personStub = PersonServiceGrpc.newStub(channel);

        Scanner input = new Scanner(System.in).useDelimiter("\n");
        System.out.println("Name: ");
        String name = input.next();

        System.out.println("CNP: ");
        String cnp = input.next();

        PersonOuterClass.Person person = new PersonOuterClass.Person();
        person.setPerson(name, cnp);

        personStub.getPerson(PersonOuterClass.PersonRequest.newBuilder().setName(name).build(), new StreamObserver<PersonOuterClass.PersonResponse>() {
        @Override
            public void onNext(PersonOuterClass.PersonResponse personResponse) {
            System.out.println(personResponse);
        }
            @Override
            public void onError(Throwable throwable) {
                System.out.println("Error : " + throwable.getMessage());
            }

            @Override
            public void onCompleted() {

            }
    } );
    }
}
