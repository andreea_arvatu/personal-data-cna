package proto.services;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import proto.PersonOuterClass;
import proto.PersonOuterClass.*;

import java.io.IOException;

import static proto.PersonOuterClass.*;

public class Main {
    public static void main(String[] args) {
        try {

            Server server = ServerBuilder.forPort(8999).addService(new Person()).build();

            server.start();

            System.out.println("Server started at " + server.getPort());

            server.awaitTermination();
        } catch (IOException e) {
            System.out.println("Error: " + e);
        } catch (InterruptedException e) {
            System.out.println("Error: " + e);
        }
    }
}
